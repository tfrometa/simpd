function varargout = Binomial(varargin)
%
% For simualtions of P(k;n,p)distributions, like Binomial one
%  

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Binomial_OpeningFcn, ...
                   'gui_OutputFcn',  @Binomial_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function Binomial_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = Binomial_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%% Input for tumor %%%%%%%%%%%%%%%%%%%%%%%%

function n_Callback(hObject, eventdata, handles)                           % Number of trials or parameter n of binomial
    global n 
    n=str2double(get(hObject,'String'));
    
function n_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p_Callback(hObject, eventdata, handles)                           % Succsess probability or parameter p of the binomial distribution
    global p 
    p=str2double(get(hObject,'String'))/100;                               % For converting from % to numeber format 
    
function p_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Simulate_Callback(hObject, eventdata, handles)
    global spSMp
    plotdists   
    
    set(handles.sprob,'String',num2str(spSMp,3));
    
function plotdists
   
    global n p spSMp     
      cla
      k = 0:1:n;
      for i=1:n+1
          binom(i) = binopdf(i-1,n,p)*100;
          ODist(i)= OD(k(i))*100;
      end
      spSMp=sum(ODist);
      pbin=plot(k,binom);  
      set(pbin,'Color','red','LineStyle',':','Marker','x')
      hold all
      pODist=plot(k,ODist);     
      set(pODist,'Color','blue','LineStyle',':','Marker','x')
      xlabel('k');
      title 'Probabilities (k;n,p)'; ylabel('Prob(%)');
      grid on
      legend('B(k;n,p)','SMp(k;n,p)');
    

function OD=OD(k)                                                          % Function determining probability of (k;n,p)
  global p n
 
  nsuck=0;                                                                 % Number of k successes 
  nfaik0=0;                                                                % Number of faiules, i.e. for k=0
  rtrials=1000;                                                            % Repeated trials  
  for j=1:rtrials 
     nsuc=0;
     for i=1:n 
         gnum=rand; 
         if gnum<p
            nsuc=nsuc+1;
         end 
     end 
     if nsuc==0
        nfaik0=nfaik0+1; 
     end    
     if nsuc==k 
         nsuck=nsuck+1;         
     end
  end 
  if k==0
      OD=nfaik0/rtrials;
  else
      OD=nsuck/rtrials;
  end
  
        
        

     
   

        
    
   
    














