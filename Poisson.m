function varargout = Poisson(varargin)
%
% For simualtions of P(x;�,Xmax)distributions, similar to Poisson one 
%  

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Poisson_OpeningFcn, ...
                   'gui_OutputFcn',  @Poisson_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function Poisson_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = Poisson_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%% Input for tumor %%%%%%%%%%%%%%%%%%%%%%%%

function mu_Callback(hObject, eventdata, handles)                          % Parameter � of the Poisson distribution 
    global mu 
    mu=str2double(get(hObject,'String'));                               
    
function mu_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Xmax_Callback(hObject, eventdata, handles)                        % Upper limit of the discrete random variable X where P>0
    global Xmax 
    Xmax=str2double(get(hObject,'String'));
    
function Xmax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Simulate_Callback(hObject, eventdata, handles)
    global spSMp
    plotdists   
    
    set(handles.sprob,'String',num2str(spSMp,3));
    
function plotdists
   
    global mu Xmax spSMp     
      cla
      p=mu/Xmax;
      k = 0:1:Xmax;
      for i=1:Xmax+1
          poiss(i) = poisspdf(i-1,mu)*100;
          ODist(i)= OD(k(i))*100;
      end
      spSMp=sum(ODist);
      ppoiss=plot(k,poiss);  
      set(ppoiss,'Color','red','LineStyle',':','Marker','x')
      hold all
      pODist=plot(k,ODist);     
      set(pODist,'Color','blue','LineStyle',':','Marker','x') 
      xlabel('x');
      title ('"Poisson distribution (PD)"'); ylabel('Prob(%)');
      grid on
      legend('PD(x;�)','SMp(x;�,Xmax)');
    

function OD=OD(k)                                                          % Function determining probability of (k;n,p)
  global mu Xmax
  p=mu/Xmax;                                                               % The success probability is calculated as p=mu/Xmax
  n=Xmax;                                                                  % The simulations are done for n=Xmax
  nsuck=0;                                                                 % Number of k successes 
  nfaik0=0;                                                                % Number of faiules, i.e. for k=0
  rtrials=10000;                                                            % Repeated trials for determining number of k successes  
  for j=1:rtrials 
     nsuc=0;
     for i=1:n 
         gnum=rand; 
         if gnum<p
            nsuc=nsuc+1;
         end 
     end 
     if nsuc==0
        nfaik0=nfaik0+1; 
     end    
     if nsuc==k 
         nsuck=nsuck+1;         
     end
  end 
  if k==0
      OD=nfaik0/rtrials;
  else
      OD=nsuck/rtrials;
  end
