function varargout = SimPD(varargin)

% This is main code that lets to access to "Binomial" and "Poisson" modules 
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimPD_OpeningFcn, ...
                   'gui_OutputFcn',  @SimPD_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimPD is made visible.
function SimPD_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);


function varargout = SimPD_OutputFcn(hObject, eventdata, handles) 


varargout{1} = handles.output;

function Pknp_Callback(hObject, eventdata, handles)                        % For simualtions of P(k;n,p)distributions, like Binomial one 
    Binomial
    
function Poisson_Callback(hObject, eventdata, handles)                     % For simualtions of P(x;µ,Xmax)distributions, similar to Poisson one 
    Poisson
